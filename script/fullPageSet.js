$(document).ready(function () {
    $('#fullpage').fullpage({

        anchors: ['page1', 'page2', 'page3', 'page4'],
        afterSlideLoad: function (anchorLink, index, slideAnchor, slideIndex) {
            var leavingSection = $(this)

            //first slide of the second section
            if (anchorLink == 'page3' && slideIndex == 1) {
                $('.contact__envelop').toggleClass('zoomInDown');
            } else if (anchorLink == 'page3' && slideIndex == 0) {
                $('.contact__envelop').removeClass('zoomInDown');
            }


        }



    });

    $(document).on('click', '#home', function () {
        $.fn.fullpage.moveTo('page1');

    });
    $(document).on('click', '#top', function () {
        $.fn.fullpage.moveTo('page1');

    });
    $(document).on('click', '#work', function () {
        $.fn.fullpage.moveTo('page2');

    });
    $(document).on('click', '#about', function () {
        $.fn.fullpage.moveTo('page3');

    });
});
