        $(document).ready(function () {

            var controller = new ScrollMagic.Controller({
                globalSceneOptions: {
                    triggerHook: 'onLeave'
                }
            });

            // get all slides
            var slides = document.querySelectorAll("dami");

            // create scene for every slide
            for (var i = 0; i < slides.length; i++) {
                new ScrollMagic.Scene({
                        triggerElement: slides[i]
                    })
                    .setPin(slides[i])
                    .addTo(controller);
            }

            let aboutTypeWritting1 = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.about__line-1-1', 'about__line-1')

            .addTo(controller);


            let aboutTypeWritting2 = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.about__line-2-2', 'about__line-2')

            .addTo(controller);






            let aboutTypeWritting3 = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.about__line-2-2', 'about__line-2')

            .addTo(controller);






            let aboutTypeWritting4 = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.about__line-3-3', 'about__line-3')

            .addTo(controller);


            let aboutTypeWritting5 = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.about__line-4-4', 'about__line-4')

            .addTo(controller);
            let aboutTypeWritting6 = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.about__line-5-5', 'about__line-5')

            .addTo(controller);




            let aboutIconsOdd = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.about__iconsBox:nth-child(odd)', 'fadeInLeftBig')

            .addTo(controller);


            let aboutIconsEven = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.about__iconsBox:nth-child(even)', 'fadeInRightBig')

            .addTo(controller);


            //-------------------typewritting for mobile version 

            let aboutMobileTypeWritting1 = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.aboutMobile__line-1-1', 'aboutMobile__line-1')

            .addTo(controller);


            let aboutMobileTypeWritting2 = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.aboutMobile__line-2-2', 'aboutMobile__line-2')

            .addTo(controller);






            let aboutMobileTypeWritting3 = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.aboutMobile__line-3-3', 'aboutMobile__line-3')

            .addTo(controller);






            let aboutMobileTypeWritting4 = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.aboutMobile__line-4-4', 'aboutMobile__line-4')

            .addTo(controller);


            let aboutMobileTypeWritting5 = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.aboutMobile__line-5-5', 'aboutMobile__line-5')

            .addTo(controller);
            let aboutMobileTypeWritting6 = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.aboutMobile__line-6-6', 'aboutMobile__line-6')

            .addTo(controller);
            let aboutMobileTypeWritting7 = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.aboutMobile__line-7-7', 'aboutMobile__line-7')

            .addTo(controller);
            
                        let aboutMobileTypeWritting8 = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.aboutMobile__line-8-8', 'aboutMobile__line-8')

            .addTo(controller);


            let aboutMobileTypeWritting9 = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.aboutMobile__line-9-9', 'aboutMobile__line-9')

            .addTo(controller);
            let aboutMobileTypeWritting10 = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.aboutMobile__line-10-10', 'aboutMobile__line-10')

            .addTo(controller);

            let aboutMobileTypeWritting11 = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.aboutMobile__line-11-11', 'aboutMobile__line-11')

            .addTo(controller);
            
              let aboutMobileIconsOdd = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.aboutMobile__iconsBox:nth-child(odd)', 'fadeInLeftBig')

            .addTo(controller);


            let aboutMobileIconsEven = new ScrollMagic.Scene({
                triggerElement: '.triggerPointForAbout'

            })

            .setClassToggle('.aboutMobile__iconsBox:nth-child(even)', 'fadeInRightBig')

            .addTo(controller);









        });
